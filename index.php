<?php include_once "pizza.php"; ?>

<!DOCTYPE html>
<html lang ="en">
	<head>
		<meta charset="utf-8"/>
		<title>Daniel Meakin | Portfolio</title>
		<link rel ="stylesheet" href ="../../main.css"/>
		<link rel ="stylesheet" href ="style.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script language ="javascript" type="text/javascript" src ="pizza.js"></script>

	</head>
	<body onload ="ajaxCall('customer');">
		<div id = "big_wrapper">
			<header id = "top_header">
				<h1>Daniel Meakin | Portfolio</h1>            
			</header>
			<nav id = "top_menu">
				<ul>
					<li><a href = "../../index.html">Home</a></li>
					<li><a href = "../../resume.html">Resume</a></li>
					<li><a href = "../../portfolio/">Portfolio</a></li>
				</ul>
			</nav>
			<section id = "section_main">
				<a id ="customerViewButton" onclick ="ajaxCall('customer');"href ="javascript: void(0)">Customer View</a> | 
				<a id ="engineerViewButton" onclick ="ajaxCall('engineer');"href ="javascript: void(0)">Pizza Engineer View</a>
				<div id ="selectedView">
					
				</div>
			</section>
		</div>
	</body>
</html>
