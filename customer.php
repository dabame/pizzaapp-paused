<?php include_once ('connection.php');

# Displays the Pizza Menu
# IMPORTANT: Function assumes is contained inside a table
# with 5 columns (pizzaName and 4 sizes)
function showMenu($conn)
{
	// get the pizza names
	$sql = "SELECT * FROM pizza";
	$pizzasResponse = $conn->query($sql);

	// for each pizza, get the total cost for the toppings
	while($row = $pizzasResponse->fetch_assoc())
	{
        // fetch the cost of the toppings
		$sql = "SELECT SUM(topping.price) AS totalToppingPrice " . 
			"FROM topping " . 
			"JOIN pizzaToppings " .
			"ON pizzaToppings.toppingName=topping.toppingName " .
			"WHERE pizzaToppings.pizzaName='".$row['pizzaName']."';";
		$toppingsCost = $conn->query($sql);
        $toppingsCost = $toppingsCost->fetch_assoc()['totalToppingPrice'];

		// display each pizza
		echo "<tr><td>".$row['pizzaName'] . "</td>";
        // fetch the cost of the sizes
        $sql = "SELECT basePrice ".
             "FROM size ".
             "ORDER BY basePrice;";
        $sizeResponse = $conn->query($sql);

		// display the different sizes cost for each pizza
		while ($sizeRow = $sizeResponse->fetch_assoc())
		{
			$sizeCost = $sizeRow['basePrice'] + $toppingsCost;
			echo "<td>$" . number_format((float)$sizeCost, 2, '.', ''). "</td>";
		}
		echo "</tr>";
	}
	
}
?>


<table id ="menu">
	<thead>
		<tr>
			<th colspan ="5">
				Pizza Palace
			</th>
		</tr>
		<tr>
			<td></td>
			<td>personal</td>
			<td>medium</td>
			<td>large</td>
			<td>mega</td>
		</tr>
	</thead>
	<tbody>
		<?php showMenu($conn); ?>
	</tbody>
</table>
