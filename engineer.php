<?php
include_once("connection.php");

// get the pizza names
$sql = "SELECT pizzaName ".
	"FROM pizza;";
$pizzaResponse = $conn->query($sql);

for ($i = 0; $row = $pizzaResponse->fetch_assoc(); $i++)
{
	// save the pizza name for later
	$_pizzas[$i] = $row['pizzaName'];

	// get the toppings associated with that pizza
	$sql = "SELECT toppingName ".
		"FROM pizzaToppings ".
		"WHERE pizzaName = '".$row['pizzaName']."';";
	$toppingResponse = $conn->query($sql);

	for ($j = 0; $row2 = $toppingResponse->fetch_assoc(); $j++)
	{
		$_toppings[$i][$j] = $row2['toppingName'];
	}

}

?>

<table>
	<thead>
		<tr>
			<th>Engineer</th>
		</tr>
		<tr>
			<td>Pizza Name</td>
			<td>Toppings</td>
		</tr>
		<?php
		for ($i = 0; $i < count($_pizzas); $i++)
		{
			echo "<tr><td>".$_pizzas[$i]."</td><td>";
			for ($j = 0; $j < count($_toppings[$i]); $j++)
			{
				echo $_toppings[$i][$j]." ";
			}
			echo "</td></tr>";
		}
		
			
		?>

	</thead>
</table>
	
